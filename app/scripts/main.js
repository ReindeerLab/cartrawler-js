window.onload = function () {

  callApi(handComponents);

  // Bind values to the Pickup/Return area
  function setPickupReturnInfo(data) {

    // Load and format Pickup Info
    var rawPickupDate = data['@PickUpDateTime'];
    var pickupDate = rawPickupDate.split('T')[0].split('-').reverse().join('/');
    var pickupTime = rawPickupDate.split('T')[1].slice(0, -4);
    var pickupLocation = data.PickUpLocation['@Name'];

    // Load and format Return Info
    var rawReturnDate = data['@ReturnDateTime'];
    var returnDate = rawReturnDate.split('T')[0].split('-').reverse().join('/');
    var returnTime = rawReturnDate.split('T')[1].slice(0, -4);
    var returnLocation = data.ReturnLocation['@Name'];

    // Function to bind values in the view
    document.getElementById('pickuplocation').innerHTML = pickupLocation;
    document.getElementById('pickupdate').innerHTML = pickupDate + ' at ' + pickupTime;
    document.getElementById('returnlocation').innerHTML = returnLocation;
    document.getElementById('returndate').innerHTML = returnDate + ' at ' + returnTime;

  }

  // Handle functions for components in the view
  function handComponents(result) {

    setPickupReturnInfo(result.VehRentalCore);

    var allVehicles = setVehicles(result.VehVendorAvails);

    allVehicles.forEach(function (cardData) {
      parseCarDetails(cardData, 'carslist', true);
    });
  }

};



