window.onload = function () {

  if (localStorage.getItem(('CarTrawlerList'))) {
    handle();
  } else {
    callApi(function(result){
      setVehicles(result.VehVendorAvails);
      handle();
    });

  }
};


function handle() {
  var list = JSON.parse(localStorage.getItem('CarTrawlerList'));

  var selectedCar = list.filter(function(car, i) {
    var carId = getUrlParameter("id").split("-");
    if(car.vendor == carId[0] && car.Vehicle["@Code"] == carId[1])
    {
      return car;
    }
  });
  parseCarDetails(selectedCar.pop(), 'cardetail', false);
}
