function callApi(callback) {
// HTTP Request
  var xhr = new XMLHttpRequest();
  xhr.open('GET', '../assets/cars.json', true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send();

// Watcher for state change
  xhr.addEventListener('readystatechange', processRequest, false);

// Callback called on state change
  function processRequest() {
    // When ready state is done - response retrieved
    if (xhr.readyState == 4 && xhr.status == 200) {
      var result = JSON.parse(xhr.responseText)[0].VehAvailRSCore;
      callback(result);
    }
  }
}

// Handle functions for components in the view
function setVehicles(result) {

  var allVehicles = [];

  result.forEach(function (value) {
    this.vendor = value.VehAvails;
    this.vendorName = value.Vendor['@Name'];
    vendor.forEach(function (vehicles) {
      this.prop = 'vendor';
      this.val = vendorName;
      vehicles[prop] = val;

      // Push all data into one array
      allVehicles.push(vehicles);
    })
  });

  // Sort array by price (the default)
  allVehicles.sort(function (a, b) {
    return parseFloat(a.TotalCharge['@RateTotalAmount']) - parseFloat(b.TotalCharge['@RateTotalAmount']);
  });

  localStorage.setItem('CarTrawlerList', JSON.stringify(allVehicles));

  return allVehicles;
}

function parseCarDetails(car, element, isMain) {

  // Set value to variables
  var carDetails = document.getElementById(element);
  var vehicleDetails = car.Vehicle;
  var totalCharge = car.TotalCharge;

  var vehicleName = vehicleDetails.VehMakeModel['@Name'];
  var vehicleImg = vehicleDetails.PictureURL;
  var vehicleTransmission = vehicleDetails['@TransmissionType'];
  var vehiclePassengers = vehicleDetails['@PassengerQuantity'];
  var vehicleFuelType = vehicleDetails['@FuelType'];
  var vehicleBaggage = vehicleDetails['@BaggageQuantity'];
  var vehicleAirCondition = vehicleDetails['@AirConditionInd'];
  var vehicleTotalPrice = '$' + totalCharge['@EstimatedTotalAmount'] + ' ' + totalCharge['@CurrencyCode'];
  var vehicleStatus = car['@Status'];
  var vehicleVendor = car.vendor;

  // Set DOM objects HTML structures
  var card = document.createElement('div');
  card.className = 'panel panel-default';

  // Create click event if page is main
  if (isMain) {
    card.setAttribute('onclick', 'seeDetails(car)');
    card.onclick = function() {seeDetails(car);};
  }

  var cardHeading = document.createElement('div');
  cardHeading.className = 'panel-heading';

  var cardTitle = document.createElement('h3');
  cardTitle.className = 'panel-title';

  var cardBody = document.createElement('div');
  cardBody.className = 'panel-body';

  var cardContentWrapper = document.createElement('div');
  cardContentWrapper.className = 'row';

  var cardImageWrapper = document.createElement('div');
  cardImageWrapper.className = 'col-xs-6 col-md-3';

  var cardImage = document.createElement('IMG');
  cardImage.setAttribute('src', vehicleImg);
  cardImage.className = 'img-responsive center-block';

  var cardSecondColumn = document.createElement('div');
  cardSecondColumn.className = 'col-xs-6 col-md-3';

  var transmissionWrapper = document.createElement('h5');
  var airWrapper = document.createElement('h5');
  var bagageWrapper = document.createElement('h5');
  var passengersWrapper = document.createElement('h5');
  var fuelWrapper = document.createElement('h5');

  var cardThirdColumn = document.createElement('div');
  cardThirdColumn.className = 'col-xs-6 col-xs-offset-6 col-md-3 col-md-offset-0';

  var statusWrapper = document.createElement('h5');
  statusWrapper.className = 'available';

  var vendorNameWrapper = document.createElement('h5');

  var cardFourthColumn = document.createElement('div');
  cardFourthColumn.className = 'col-xs-12 col-md-3';

  var priceWrapper = document.createElement('div');
  priceWrapper.className = 'list-group';

  var listGroupItem = document.createElement('div');
  listGroupItem.className = 'list-group-item';

  var listGroupItemHeading = document.createElement('h4');
  listGroupItemHeading.className = 'list-group-item-heading';

  var listGroupItemText = document.createElement('p');
  listGroupItemText.className = 'list-group-item-text';

  // Layout structures
  carDetails.appendChild(card);
  card.appendChild(cardHeading);
  cardHeading.appendChild(cardTitle);
  card.appendChild(cardBody);
  cardBody.appendChild(cardContentWrapper);

  cardContentWrapper.appendChild(cardImageWrapper);
  cardImageWrapper.appendChild(cardImage);

  cardContentWrapper.appendChild(cardSecondColumn);
  cardSecondColumn.appendChild(transmissionWrapper);
  cardSecondColumn.appendChild(airWrapper);
  cardSecondColumn.appendChild(bagageWrapper);
  cardSecondColumn.appendChild(passengersWrapper);
  cardSecondColumn.appendChild(fuelWrapper);

  cardContentWrapper.appendChild(cardThirdColumn);
  cardThirdColumn.appendChild(statusWrapper);
  cardThirdColumn.appendChild(vendorNameWrapper);

  cardContentWrapper.appendChild(cardFourthColumn);
  cardFourthColumn.appendChild(priceWrapper);
  priceWrapper.appendChild(listGroupItem);
  listGroupItem.appendChild(listGroupItemText);
  listGroupItem.appendChild(listGroupItemHeading);

  cardTitle.appendChild(document.createTextNode(vehicleName));

  // Insert content in layout structures
  transmissionWrapper.appendChild(document.createTextNode('Transmission: ' + vehicleTransmission));
  airWrapper.appendChild(document.createTextNode('Air Condition: ' + vehicleAirCondition));
  bagageWrapper.appendChild(document.createTextNode('Baggage Quantity: ' + vehicleBaggage));
  passengersWrapper.appendChild(document.createTextNode('Seats: ' + vehiclePassengers));
  fuelWrapper.appendChild(document.createTextNode('Fuel Type: ' + vehicleFuelType));
  listGroupItemText.appendChild(document.createTextNode('Estimated Total Ammount'));
  listGroupItemHeading.appendChild(document.createTextNode(vehicleTotalPrice));
  statusWrapper.appendChild(document.createTextNode('Status: ' + vehicleStatus));
  vendorNameWrapper.appendChild(document.createTextNode('Vendor: ' + vehicleVendor));
}

// Function that set information for the detail page
function seeDetails(vehicle) {
  // Sent to detail page
  window.location.href = 'detail.html?id='+[vehicle.vendor, vehicle.Vehicle["@Code"]].join("-");
}

// Filter query in the url
function getUrlParameter(name) {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  var results = regex.exec(location.search);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};
