## The project
This is a VanillaJS project.<br>
Simply open index.html to view the application

Run `npm install` to install grunt if you wish to serve it with livereload.<br>
Run `grunt server` for preview.<br>

Controllers:
* [main] - Responsible for the main page
* [detail] - Responsible for the detailed widget page

Services:
* [lib] - Serves all functions and variables utilised in both Controllers 

Renato<br>
<a href="reindeerlab.co" target="_blank">reindeerlab.co</a>